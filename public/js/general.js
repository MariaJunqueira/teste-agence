$(document).ready(function() {
  consultoresSelecionados = []
  clientesSelecionados = []

  $('#multiple-consultor').multiselect({
    // includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 200,
    onChange: function(option, checked, select) {
      flag = consultoresSelecionados.indexOf($(option).val())
      if(flag == "-1") {
        consultoresSelecionados.push($(option).val());
      } else {
        consultoresSelecionados.splice(flag, 1)
      }
    }
  });

  $('#multiple-cliente').multiselect({
    // includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 200,
    onChange: function(option, checked, select) {
      flag = clientesSelecionados.indexOf($(option).val())
      if(flag == "-1") {
        clientesSelecionados.push($(option).val());
      } else {
        clientesSelecionados.splice(flag, 1)
      }
    }
  });

  $('.multiselect-clear-filter').html('<i class="fas fa-times-circle"></i>')

});

var url_padrao = "http://localhost/teste-agence/public/"


function getColor() {
  red = Math.floor((Math.random() * 255));
  green = Math.floor((Math.random() * 255));
  blue = Math.floor((Math.random() * 255));
  return 'rgba('+red+','+green+','+blue+'1)'
}

function changeMonth(mes) {
  if(mes == 1)
  return 'janeiro'
  if(mes == 2)
  return 'fevereiro'
  if(mes == 3)
  return 'março'
  if(mes == 4)
  return 'abril'
  if(mes == 5)
  return 'maio'
  if(mes == 6)
  return 'junho'
  if(mes == 7)
  return 'julho'
  if(mes == 8)
  return 'agosto'
  if(mes == 9)
  return 'setembro'
  if(mes == 10)
  return 'outubro'
  if(mes == 11)
  return 'novembro'
  if(mes == 12)
  return 'dezembro'
}
function geraRelatorio(pessoa) {
  var datas = []


  if(pessoa == 'consultor') {
    mes1 = $("[name=mes_inicial]").val()
    ano1 = $("[name=ano_inicial]").val()
    mes2 = $("[name=mes_final]").val()
    ano2 = $("[name=ano_final]").val()
    datas.push(mes1,ano1,mes2,ano2)
    if(consultoresSelecionados.length > 0) {
      $.get(url_padrao+"gera-relatorio/"+consultoresSelecionados+"/"+datas+"/"+pessoa+"/false", function(data, status){
        $(".relatorio-consultor").html(data);
      });
    }else {
      swal(
        'Opss!',
        'Você precisa selecionar ao menos 1 consultor',
        'warning'
      )
    }
  } else {
    mes1 = $("[name=mes_inicial_cliente]").val()
    ano1 = $("[name=ano_inicial_cliente]").val()
    mes2 = $("[name=mes_final_cliente]").val()
    ano2 = $("[name=ano_final_cliente]").val()
    datas.push(mes1,ano1,mes2,ano2)
    if(clientesSelecionados.length > 0) {
      $.get(url_padrao+"gera-relatorio/"+clientesSelecionados+"/"+datas+"/"+pessoa+"/false", function(data, status){
        $(".relatorio-cliente").html(data);
      });
    }else {
      swal(
        'Opss!',
        'Você precisa selecionar ao menos 1 cliente',
        'warning'
      )
    }
  }
}

var temp;
function geraGrafico(pessoa, tipo) {
  var datas = []
  mes1 = $("[name=mes_inicial]").val()
  ano1 = $("[name=ano_inicial]").val()
  mes2 = $("[name=mes_final]").val()
  ano2 = $("[name=ano_final]").val()
  datas.push(mes1,ano1,mes2,ano2)

  if(pessoa == 'consultor') {
    if(consultoresSelecionados.length > 0) {
      $.get(url_padrao+"gera-relatorio/"+consultoresSelecionados+"/"+datas+"/"+pessoa+"/true", function(data, status){
        nome = []
        receita = []
        custo_fixo = []
        cor = []
        salario = 0
        j = 0
        for(i=0; i < data['fatura'].length; i++) {
          if (typeof(data['fatura'][i][0]) !== 'undefined') {
            receita_liquida_total = 0
            receita_liquida = data['fatura'][i]['receita_liquida']
            for (var key in receita_liquida) {
              receita_liquida_total += receita_liquida[key];
            }
            receita[j] = Math.round(receita_liquida_total*100)/100;
            nome[j] = data['fatura'][i][0]['no_usuario']
            j++
          }
          salario += Math.round(data['fatura'][i]['custo_fixo']*100)/100;
        }
        salario = (salario/j)
        for(i = 0; i < j; i++)
        {
          custo_fixo[i] = salario
          cor[i] = getColor()
        }
        var ctx = document.getElementById("grafico-consultor").getContext("2d");

        mes1 = changeMonth(mes1)
        mes2 = changeMonth(mes2)

        if(tipo == 'barra') {
          var mixedChart = new Chart(ctx, {
            type: 'bar',
            data: {
              datasets: [{
                label: 'Receita Líquida',
                data: receita,
                backgroundColor: cor
              }, {
                label: 'Custo Fixo',
                data: custo_fixo,

                // Changes this dataset to become a line
                type: 'line'
              }],
              labels: nome
            },
            options: {
              title: {
                display: true,
                text: 'Consultores de '+ mes1 + ' de ' + ano1 + ' à ' + mes2+ ' de '+ ano2
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero:true
                  }
                }]
              }
            }
          });
        } else if(tipo == 'pizza') {
          var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: {
              datasets: [{
                data: receita,
                backgroundColor: cor
              }],
              labels: nome
            },
            options:{
              title: {
                display: true,
                text: 'Consultores de '+ mes1 + ' de ' + ano1 + ' à ' + mes2+ ' de '+ ano2
              }
            }
          });
        };
      })
    } else {
      swal(
        'Opss!',
        'Você precisa selecionar ao menos 1 consultor',
        'warning'
      )
    }
  } else {
    if(clientesSelecionados.length > 0) {
      var datas = []
      mes1 = $("[name=mes_inicial_cliente]").val()
      ano1 = $("[name=ano_inicial_cliente]").val()
      mes2 = $("[name=mes_final_cliente]").val()
      ano2 = $("[name=ano_final_cliente]").val()
      datas.push(mes1,ano1,mes2,ano2)
      $.get(url_padrao+"gera-relatorio/"+clientesSelecionados+"/"+datas+"/"+pessoa+"/true", function(data, status){
        temp = data;
        nome = []
        receita = []
        custo_fixo = []
        cor = []
        salario = 0
        j = 0

        for(var i in temp["fatura"]) {
          if(temp["fatura"].hasOwnProperty(i)) {
            if (typeof(data['fatura'][i]) !== 'undefined') {
              receita_liquida_total = 0
              receita_liquida = data['fatura'][i]
              for (var key in receita_liquida) {
                if((typeof(receita_liquida[key]['valor']) != "undefined") && (typeof(receita_liquida[key]['total_imp_inc']) != "undefined")) {
                  receita_liquida_total += receita_liquida[key]['valor'] - (receita_liquida[key]['valor']* receita_liquida[key]['total_imp_inc'] /100);
                }
              }
              receita[j] = Math.round(receita_liquida_total*100)/100;
              nome[j] = data['fatura'][i][0]['no_fantasia']
              j++
            }
          }
        }

        for(i = 0; i < j; i++)
        {
          custo_fixo[i] = salario
          cor[i] = getColor()
        }
        var ctx = document.getElementById("grafico-cliente").getContext("2d");

        mes1 = changeMonth(mes1)
        mes2 = changeMonth(mes2)

        if(tipo == 'barra') {
          var mixedChart = new Chart(ctx, {
            type: 'bar',
            data: {
              datasets: [{
                label: 'Receita Líquida',
                data: receita,

                // Changes this dataset to become a line
                type: 'line'
              }],
              labels: nome
            },
            options: {
              title: {
                display: true,
                text: 'Clientes de '+ mes1 + ' de ' + ano1 + ' à ' + mes2+ ' de '+ ano2
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero:true
                  }
                }]
              }
            }
          });
        } else if(tipo == 'pizza') {
          var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: {
              datasets: [{
                data: receita,
                backgroundColor: cor
              }],
              labels: nome
            },
            options:{
              title: {
                display: true,
                text: 'Clientes de '+ mes1 + ' de ' + ano1 + ' à ' + mes2+ ' de '+ ano2
              }
            }
          });
        };
        // $(".relatorio-cliente").html(data);
      });
    }else {
      swal(
        'Opss!',
        'Você precisa selecionar ao menos 1 cliente',
        'warning'
      )
    }
  }
}

function getValoresReceita(element, index, array) {
  console.log("a[" + index + "] = " + element);
}

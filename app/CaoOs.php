<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaoOs extends Model
{
  protected $table = 'cao_os';
  protected $primaryKey = 'id';
  protected $fillable = ['co_os', 'nu_os', 'co_sistema', 'co_usuario', 'co_arquitetura', 'ds_os', 'ds_caracteristica', 'ds_requisito', 'dt_inicio', 'dt_fim', 'co_status', 'diretoria_sol', 'dt_sol', 'nu_tel_sol', 'ddd_tel_sol', 'usuario_sol', 'dt_imp', 'dt_garantia', 'co_email', 'co_os_prospect_rel'];

  public static function getValoresFaturaConsultor($usuario, $data_inicial, $data_final)
  {
    for($i = 0; $i < count($usuario); $i++) {
      $dados['fatura'][$i] = CaoOs::where('cao_os.co_usuario', '=', $usuario[$i])
      ->join('cao_fatura', function ($join) use ($data_inicial, $data_final) {
        $join->whereBetween('cao_fatura.data_emissao', [$data_inicial, $data_final])
        ->on('cao_fatura.co_os', 'cao_os.co_os');
      })
      ->join('cao_usuario', 'cao_usuario.co_usuario', 'cao_os.co_usuario')
      ->orderBy('cao_fatura.data_emissao')
      ->select('cao_os.co_usuario','cao_usuario.no_usuario', 'cao_fatura.valor',
      'cao_fatura.total_imp_inc', 'cao_fatura.data_emissao', 'cao_fatura.comissao_cn')
      ->get();
    }
    return $dados;
  }

  public static function getValoresFaturaCliente($usuario, $data_inicial, $data_final)
  {
    for($i = 0; $i < count($usuario); $i++) {
      $dados['fatura'][$i] = CaoFatura::where('cao_fatura.co_cliente', '=', $usuario[$i])
      ->whereBetween('cao_fatura.data_emissao', [$data_inicial, $data_final])
      ->join('cao_cliente', 'cao_cliente.co_cliente', 'cao_fatura.co_cliente')
      ->orderBy('cao_fatura.data_emissao')
      ->select('cao_fatura.co_cliente','cao_cliente.no_fantasia', 'cao_fatura.valor',
      'cao_fatura.total_imp_inc', 'cao_fatura.data_emissao', 'cao_fatura.comissao_cn')
      ->get();
    }
    return $dados;
  }

  
}

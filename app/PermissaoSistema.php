<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissaoSistema extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissao_sistema';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['co_usuario', 'co_tipo_usuario', 'co_sistema', 'in_ativo', 'co_usuario_atualizacao', 'dt_atualizacao'];


}

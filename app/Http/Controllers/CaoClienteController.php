<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CaoCliente;
use Illuminate\Http\Request;

class CaoClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $caocliente = CaoCliente::where('co_cliente', 'LIKE', "%$keyword%")
                ->orWhere('no_razao', 'LIKE', "%$keyword%")
                ->orWhere('no_fantasia', 'LIKE', "%$keyword%")
                ->orWhere('no_contato', 'LIKE', "%$keyword%")
                ->orWhere('nu_telefone', 'LIKE', "%$keyword%")
                ->orWhere('nu_ramal', 'LIKE', "%$keyword%")
                ->orWhere('nu_cnpj', 'LIKE', "%$keyword%")
                ->orWhere('ds_endereco', 'LIKE', "%$keyword%")
                ->orWhere('nu_numero', 'LIKE', "%$keyword%")
                ->orWhere('ds_complemento', 'LIKE', "%$keyword%")
                ->orWhere('no_bairro', 'LIKE', "%$keyword%")
                ->orWhere('nu_cep', 'LIKE', "%$keyword%")
                ->orWhere('no_pais', 'LIKE', "%$keyword%")
                ->orWhere('co_ramo', 'LIKE', "%$keyword%")
                ->orWhere('co_cidade', 'LIKE', "%$keyword%")
                ->orWhere('co_status', 'LIKE', "%$keyword%")
                ->orWhere('ds_site', 'LIKE', "%$keyword%")
                ->orWhere('ds_email', 'LIKE', "%$keyword%")
                ->orWhere('ds_cargo_contato', 'LIKE', "%$keyword%")
                ->orWhere('tp_cliente', 'LIKE', "%$keyword%")
                ->orWhere('ds_referencia', 'LIKE', "%$keyword%")
                ->orWhere('co_complemento_status', 'LIKE', "%$keyword%")
                ->orWhere('nu_fax', 'LIKE', "%$keyword%")
                ->orWhere('ddd2', 'LIKE', "%$keyword%")
                ->orWhere('telefone2', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $caocliente = CaoCliente::latest()->paginate($perPage);
        }

        return view('cao-cliente.index', compact('caocliente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cao-cliente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        CaoCliente::create($requestData);

        return redirect('cao-cliente')->with('flash_message', 'CaoCliente added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $caocliente = CaoCliente::findOrFail($id);

        return view('cao-cliente.show', compact('caocliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $caocliente = CaoCliente::findOrFail($id);

        return view('cao-cliente.edit', compact('caocliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $caocliente = CaoCliente::findOrFail($id);
        $caocliente->update($requestData);

        return redirect('cao-cliente')->with('flash_message', 'CaoCliente updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CaoCliente::destroy($id);

        return redirect('cao-cliente')->with('flash_message', 'CaoCliente deleted!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CaoSalario;
use Illuminate\Http\Request;

class CaoSalarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $caosalario = CaoSalario::where('co_usuario', 'LIKE', "%$keyword%")
                ->orWhere('dt_alteracao_cn', 'LIKE', "%$keyword%")
                ->orWhere('brut_salario', 'LIKE', "%$keyword%")
                ->orWhere('liq_salario', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $caosalario = CaoSalario::latest()->paginate($perPage);
        }

        return view('cao-salario.index', compact('caosalario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cao-salario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        CaoSalario::create($requestData);

        return redirect('cao-salario')->with('flash_message', 'CaoSalario added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $caosalario = CaoSalario::findOrFail($id);

        return view('cao-salario.show', compact('caosalario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $caosalario = CaoSalario::findOrFail($id);

        return view('cao-salario.edit', compact('caosalario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $caosalario = CaoSalario::findOrFail($id);
        $caosalario->update($requestData);

        return redirect('cao-salario')->with('flash_message', 'CaoSalario updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CaoSalario::destroy($id);

        return redirect('cao-salario')->with('flash_message', 'CaoSalario deleted!');
    }
}

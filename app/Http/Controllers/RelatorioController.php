<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CaoOs;
use App\CaoSalario;
use App\CaoUsuario;
use App\PermissaoSistema;
use Illuminate\Http\Request;

class RelatorioController extends Controller {
  public function getRelatorio($usuarios, $datas, $pessoa, $grafico) {
    if($pessoa == 'consultor') {
      $meses = array();
      $usuario = explode(",", $usuarios);
      $data = explode(",", $datas);

      $data_inicial = $data[1]."-".$data[0]."-01";
      $data_final = $data[3]."-".$data[2]."-31";
      $relatorio = CaoOs::getValoresFaturaConsultor($usuario, $data_inicial, $data_final);

      $soma_valor_liquido = array();
      $soma_comissao = array();

      for($i = 0; $i < count($relatorio['fatura']); $i++) {
        $soma_valor_liquido[$i] = array();
        $soma_comissao[$i] = array();
        for($j = 0; $j < count($relatorio['fatura'][$i]); $j++) {
          $mes = date('m', strtotime($relatorio['fatura'][$i][$j]['data_emissao']));
          $ano = date('Y', strtotime($relatorio['fatura'][$i][$j]['data_emissao']));
          $valor = $relatorio['fatura'][$i][$j]['valor'];
          $imposto = $relatorio['fatura'][$i][$j]['total_imp_inc'];
          $comissao = $relatorio['fatura'][$i][$j]['comissao_cn'];
          $valor_liquido = ($valor - ($valor * $imposto / 100));
          $relatorio['fatura'][$i][$j]['data_emissao'] = $mes.'/'.$ano;
          $relatorio['fatura'][$i][$j]['valor_liquido'] = $valor_liquido;
          $relatorio['fatura'][$i][$j]['valor_comissao'] = ($valor_liquido - ($valor_liquido * $comissao / 100));
        }

        for($j = 0; $j < count($relatorio['fatura'][$i]); $j++) {
          $indice = $relatorio['fatura'][$i][$j]['data_emissao'];
          $valor = $relatorio['fatura'][$i][$j]['valor_liquido'];
          $comissao = $relatorio['fatura'][$i][$j]['comissao_cn'];
          $flag = 'false';
          if(!array_key_exists($indice, $soma_valor_liquido[$i])) {
            $soma_valor_liquido[$i] += array($indice.'' => $valor);
            $soma_comissao[$i] += array($indice.'' => $comissao);
          } else {
            $soma_valor_liquido[$i][$indice] += $valor;
            $soma_comissao[$i][$indice] += $comissao;
          }
        }
        $relatorio['fatura'][$i]['receita_liquida'] = $soma_valor_liquido[$i];
        $relatorio['fatura'][$i]['comissao_total'] = $soma_comissao[$i];


        $salario = CaoSalario::getSalario($usuario[$i]);
        if(count($salario) > 0) {
          $relatorio['fatura'][$i]['custo_fixo'] = $salario[0]->brut_salario;
        } else {
          $relatorio['fatura'][$i]['custo_fixo'] = 0;
        }
      }
      if($grafico == "true") {
        return response()->json($relatorio);
      } else {
        return view('relatorio.relatorio', compact('relatorio'));
      }
    } else if($pessoa = 'cliente') {

      $meses = array();
      $usuario = explode(",", $usuarios);
      $data = explode(",", $datas);

      if($data[1] == $data[3]) {
        $meses[$data[1]] = array();
        for($i = $data[0]; $i <= $data[2] ;$i++) {
          array_push($meses[$data[1]], $i);
        }
      } else if($data[1] < $data[3]) {
        for($j = $data[1]; $j <= $data[3]; $j++) {
          $meses[$j] = array();
          if($j == $data[1]) {
            for($i = $data[0]; $i <= 12 ;$i++) {
              array_push($meses[$j], $i);
            }
          } else if($j < $data[3]) {
            for($i = 1; $i <= 12 ;$i++) {
              array_push($meses[$j], $i);
            }
          } else if($j == $data[3]) {
            for($i = 1; $i <= $data[2]; $i++) {
              array_push($meses[$j], $i);
            }
          }
        }
      }


      $data_inicial = $data[1]."-".$data[0]."-01";
      $data_final = $data[3]."-".$data[2]."-31";
      $relatorio = CaoOs::getValoresFaturaCliente($usuario, $data_inicial, $data_final);
      // print_r($soma_valor_liquido); exit();

      $soma_valor_liquido = array();
      $soma_comissao = array();

      // echo count($relatorio['fatura']);
      for($i = 0; $i < count($relatorio['fatura']); $i++) {
        $soma_valor_liquido[$i] = array();
        if($data[1] == $data[3]) {
          // echo $i;
          for($n = $data[0]; $n <= $data[2] ;$n++) {
            $soma_valor_liquido[$i][sprintf("%02d", $n).'/'.$data[1]] = 0;
          }
        } else if($data[1] < $data[3]) {
          for($m = $data[1]; $m <= $data[3]; $m++) {
            $meses[$j] = array();
            if($m == $data[1]) {
              for($n = $data[0]; $n <= 12 ;$n++) {
                $soma_valor_liquido[$i][sprintf("%02d", $n).'/'.$m] = 0;
              }
            } else if($m < $data[3]) {
              for($n = 1; $n <= 12 ;$n++) {
                $soma_valor_liquido[$i][sprintf("%02d", $n).'/'.$m] = 0;
              }
            } else if($m == $data[3]) {
              for($n = 1; $n <= $data[2]; $n++) {
                $soma_valor_liquido[$i][sprintf("%02d", $n).'/'.$m] = 0;
              }
            }
          }
        }
        // echo '<pre>';
        // print_r($soma_valor_liquido[$i]);
        $soma_comissao[$i] = array();


        for($j = 0; $j < count($relatorio['fatura'][$i]); $j++) {
          $mes = date('m', strtotime($relatorio['fatura'][$i][$j]['data_emissao']));
          $ano = date('Y', strtotime($relatorio['fatura'][$i][$j]['data_emissao']));
          $valor = $relatorio['fatura'][$i][$j]['valor'];
          $imposto = $relatorio['fatura'][$i][$j]['total_imp_inc'];
          $comissao = $relatorio['fatura'][$i][$j]['comissao_cn'];
          $valor_liquido = ($valor - ($valor * $imposto / 100));
          $relatorio['fatura'][$i][$j]['data_emissao'] = $mes.'/'.$ano;
          $relatorio['fatura'][$i][$j]['valor_liquido'] = $valor_liquido;
          $relatorio['fatura'][$i][$j]['valor_comissao'] = ($valor_liquido - ($valor_liquido * $comissao / 100));
        }


        for($j = 0; $j < count($relatorio['fatura'][$i]); $j++) {
          $indice = $relatorio['fatura'][$i][$j]['data_emissao'];
          $valor = $relatorio['fatura'][$i][$j]['valor_liquido'];
          if(!array_key_exists($indice, $soma_valor_liquido[$i])) {
            $soma_valor_liquido[$i] += array($indice.'' => $valor);
          } else {
            $soma_valor_liquido[$i][$indice] += $valor;
          }
        }
        $relatorio['fatura'][$i]['receita_liquida'] = $soma_valor_liquido[$i];
        // echo '<pre>';
        // print_r($relatorio['fatura'][0]);
        if(!isset($relatorio['fatura'][$i][0]['no_fantasia'])) {
          unset($relatorio['fatura'][$i]);
        }
      }
      // echo '<pre>';
      // print_r($relatorio);exit();
      // $relatorio['fatura'][$i]['receita_liquida']['meses'] = $meses;

      if($grafico == "true") {
        return response()->json($relatorio);
      } else {
        return view('relatorio.relatorio-cliente', compact('relatorio', 'meses'));
      }
    }
  }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CaoO;
use Illuminate\Http\Request;

class CaoOsController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $caoos = CaoO::where('co_os', 'LIKE', "%$keyword%")
                ->orWhere('nu_os', 'LIKE', "%$keyword%")
                ->orWhere('co_sistema', 'LIKE', "%$keyword%")
                ->orWhere('co_usuario', 'LIKE', "%$keyword%")
                ->orWhere('co_arquitetura', 'LIKE', "%$keyword%")
                ->orWhere('ds_os', 'LIKE', "%$keyword%")
                ->orWhere('ds_caracteristica', 'LIKE', "%$keyword%")
                ->orWhere('ds_requisito', 'LIKE', "%$keyword%")
                ->orWhere('dt_inicio', 'LIKE', "%$keyword%")
                ->orWhere('dt_fim', 'LIKE', "%$keyword%")
                ->orWhere('co_status', 'LIKE', "%$keyword%")
                ->orWhere('diretoria_sol', 'LIKE', "%$keyword%")
                ->orWhere('dt_sol', 'LIKE', "%$keyword%")
                ->orWhere('nu_tel_sol', 'LIKE', "%$keyword%")
                ->orWhere('ddd_tel_sol', 'LIKE', "%$keyword%")
                ->orWhere('usuario_sol', 'LIKE', "%$keyword%")
                ->orWhere('dt_imp', 'LIKE', "%$keyword%")
                ->orWhere('dt_garantia', 'LIKE', "%$keyword%")
                ->orWhere('co_email', 'LIKE', "%$keyword%")
                ->orWhere('co_os_prospect_rel', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $caoos = CaoO::latest()->paginate($perPage);
        }

        return view('cao-os.index', compact('caoos'));
    }

    public function create()
    {
        return view('cao-os.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        CaoO::create($requestData);

        return redirect('cao-os')->with('flash_message', 'CaoO added!');
    }

    public function show($id)
    {
        $caoo = CaoO::findOrFail($id);

        return view('cao-os.show', compact('caoo'));
    }

    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $caoo = CaoO::findOrFail($id);
        $caoo->update($requestData);

        return redirect('cao-os')->with('flash_message', 'CaoO updated!');
    }

    public function destroy($id)
    {
        CaoO::destroy($id);

        return redirect('cao-os')->with('flash_message', 'CaoO deleted!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CaoFatura;
use Illuminate\Http\Request;

class CaoFaturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $caofatura = CaoFatura::where('co_fatura', 'LIKE', "%$keyword%")
                ->orWhere('co_cliente', 'LIKE', "%$keyword%")
                ->orWhere('co_sistema', 'LIKE', "%$keyword%")
                ->orWhere('co_os', 'LIKE', "%$keyword%")
                ->orWhere('num_nf', 'LIKE', "%$keyword%")
                ->orWhere('total', 'LIKE', "%$keyword%")
                ->orWhere('valor', 'LIKE', "%$keyword%")
                ->orWhere('dt_emissao', 'LIKE', "%$keyword%")
                ->orWhere('corpo_nf', 'LIKE', "%$keyword%")
                ->orWhere('comissao_cn', 'LIKE', "%$keyword%")
                ->orWhere('total_imp_inc', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $caofatura = CaoFatura::latest()->paginate($perPage);
        }

        return view('cao-fatura.index', compact('caofatura'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cao-fatura.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        CaoFatura::create($requestData);

        return redirect('cao-fatura')->with('flash_message', 'CaoFatura added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $caofatura = CaoFatura::findOrFail($id);

        return view('cao-fatura.show', compact('caofatura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $caofatura = CaoFatura::findOrFail($id);

        return view('cao-fatura.edit', compact('caofatura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $caofatura = CaoFatura::findOrFail($id);
        $caofatura->update($requestData);

        return redirect('cao-fatura')->with('flash_message', 'CaoFatura updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CaoFatura::destroy($id);

        return redirect('cao-fatura')->with('flash_message', 'CaoFatura deleted!');
    }
}

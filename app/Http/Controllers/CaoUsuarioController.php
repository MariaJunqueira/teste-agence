<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CaoCliente;
use App\CaoUsuario;
use App\PermissaoSistema;
use Illuminate\Http\Request;

class CaoUsuarioController extends Controller
{
  public function index(Request $request)
  {
    $dados['consultor'] = CaoUsuario::selectInicial();
    $dados['cliente'] = CaoCliente::selectInicial();
    return view('cao-usuario.index', compact('dados'));
  }

  public function create()
  {
    return view('cao-usuario.create');
  }

  public function store(Request $request)
  {

    $requestData = $request->all();

    CaoUsuario::create($requestData);

    return redirect('cao-usuario')->with('flash_message', 'CaoUsuario added!');
  }

  public function show($id)
  {
    $caousuario = CaoUsuario::findOrFail($id);

    return view('cao-usuario.show', compact('caousuario'));
  }

  public function edit($id)
  {
    $caousuario = CaoUsuario::findOrFail($id);

    return view('cao-usuario.edit', compact('caousuario'));
  }

  public function update(Request $request, $id)
  {

    $requestData = $request->all();

    $caousuario = CaoUsuario::findOrFail($id);
    $caousuario->update($requestData);

    return redirect('cao-usuario')->with('flash_message', 'CaoUsuario updated!');
  }

  public function destroy($id)
  {
    CaoUsuario::destroy($id);

    return redirect('cao-usuario')->with('flash_message', 'CaoUsuario deleted!');
  }
}

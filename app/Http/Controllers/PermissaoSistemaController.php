<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PermissaoSistema;
use Illuminate\Http\Request;

class PermissaoSistemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $permissaosistema = PermissaoSistema::where('co_usuario', 'LIKE', "%$keyword%")
                ->orWhere('co_tipo_usuario', 'LIKE', "%$keyword%")
                ->orWhere('co_sistema', 'LIKE', "%$keyword%")
                ->orWhere('in_ativo', 'LIKE', "%$keyword%")
                ->orWhere('co_usuario_atualizacao', 'LIKE', "%$keyword%")
                ->orWhere('dt_atualizacao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $permissaosistema = PermissaoSistema::latest()->paginate($perPage);
        }

        return view('permissao-sistema.index', compact('permissaosistema'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('permissao-sistema.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        PermissaoSistema::create($requestData);

        return redirect('permissao-sistema')->with('flash_message', 'PermissaoSistema added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $permissaosistema = PermissaoSistema::findOrFail($id);

        return view('permissao-sistema.show', compact('permissaosistema'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $permissaosistema = PermissaoSistema::findOrFail($id);

        return view('permissao-sistema.edit', compact('permissaosistema'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $permissaosistema = PermissaoSistema::findOrFail($id);
        $permissaosistema->update($requestData);

        return redirect('permissao-sistema')->with('flash_message', 'PermissaoSistema updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PermissaoSistema::destroy($id);

        return redirect('permissao-sistema')->with('flash_message', 'PermissaoSistema deleted!');
    }
}

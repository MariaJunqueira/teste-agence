<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaoSalario extends Model
{
    protected $table = 'cao_salario';
    protected $primaryKey = 'id';
    protected $fillable = ['co_usuario', 'dt_alteracao_cn', 'brut_salario', 'liq_salario'];

    public static function getSalario($usuario)
    {
       return CaoSalario::where('cao_salario.co_usuario', '=', $usuario)
        ->select('cao_salario.brut_salario')
        ->get();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaoFatura extends Model
{
    protected $table = 'cao_fatura';
    protected $primaryKey = 'id';
    protected $fillable = ['co_fatura', 'co_cliente', 'co_sistema', 'co_os', 'num_nf', 'total', 'valor', 'dt_emissao', 'corpo_nf', 'comissao_cn', 'total_imp_inc'];


}

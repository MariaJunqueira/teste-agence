<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaoCliente extends Model
{
    protected $table = 'cao_cliente';
    protected $primaryKey = 'id';
    protected $fillable = ['co_cliente', 'no_razao', 'no_fantasia', 'no_contato', 'nu_telefone', 'nu_ramal', 'nu_cnpj', 'ds_endereco', 'nu_numero', 'ds_complemento', 'no_bairro', 'nu_cep', 'no_pais', 'co_ramo', 'co_cidade', 'co_status', 'ds_site', 'ds_email', 'ds_cargo_contato', 'tp_cliente', 'ds_referencia', 'co_complemento_status', 'nu_fax', 'ddd2', 'telefone2'];

    public static function selectInicial()
    {
      return CaoCliente::where('cao_cliente.tp_cliente', '=', 'A')
      ->select('cao_cliente.co_cliente','cao_cliente.no_fantasia')
      ->orderBy('cao_cliente.no_fantasia')
      ->get();
    }

}

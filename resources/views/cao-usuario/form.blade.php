<div class="form-group {{ $errors->has('co_usuario') ? 'has-error' : ''}}">
    <label for="co_usuario" class="control-label">{{ 'Co Usuario' }}</label>
    <input class="form-control" name="co_usuario" type="text" id="co_usuario" value="{{ $caousuario->co_usuario or ''}}" >
    {!! $errors->first('co_usuario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_senha') ? 'has-error' : ''}}">
    <label for="ds_senha" class="control-label">{{ 'Ds Senha' }}</label>
    <input class="form-control" name="ds_senha" type="text" id="ds_senha" value="{{ $caousuario->ds_senha or ''}}" >
    {!! $errors->first('ds_senha', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_usuario_autorizacao') ? 'has-error' : ''}}">
    <label for="co_usuario_autorizacao" class="control-label">{{ 'Co Usuario Autorizacao' }}</label>
    <input class="form-control" name="co_usuario_autorizacao" type="text" id="co_usuario_autorizacao" value="{{ $caousuario->co_usuario_autorizacao or ''}}" >
    {!! $errors->first('co_usuario_autorizacao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_matricula') ? 'has-error' : ''}}">
    <label for="nu_matricula" class="control-label">{{ 'Nu Matricula' }}</label>
    <input class="form-control" name="nu_matricula" type="number" id="nu_matricula" value="{{ $caousuario->nu_matricula or ''}}" >
    {!! $errors->first('nu_matricula', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_nascimento') ? 'has-error' : ''}}">
    <label for="dt_nascimento" class="control-label">{{ 'Dt Nascimento' }}</label>
    <input class="form-control" name="dt_nascimento" type="date" id="dt_nascimento" value="{{ $caousuario->dt_nascimento or ''}}" >
    {!! $errors->first('dt_nascimento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_admissao_empresa') ? 'has-error' : ''}}">
    <label for="dt_admissao_empresa" class="control-label">{{ 'Dt Admissao Empresa' }}</label>
    <input class="form-control" name="dt_admissao_empresa" type="date" id="dt_admissao_empresa" value="{{ $caousuario->dt_admissao_empresa or ''}}" >
    {!! $errors->first('dt_admissao_empresa', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_inclusao') ? 'has-error' : ''}}">
    <label for="dt_inclusao" class="control-label">{{ 'Dt Inclusao' }}</label>
    <input class="form-control" name="dt_inclusao" type="datetime-local" id="dt_inclusao" value="{{ $caousuario->dt_inclusao or ''}}" >
    {!! $errors->first('dt_inclusao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_expiracao') ? 'has-error' : ''}}">
    <label for="dt_expiracao" class="control-label">{{ 'Dt Expiracao' }}</label>
    <input class="form-control" name="dt_expiracao" type="date" id="dt_expiracao" value="{{ $caousuario->dt_expiracao or ''}}" >
    {!! $errors->first('dt_expiracao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_cpf') ? 'has-error' : ''}}">
    <label for="nu_cpf" class="control-label">{{ 'Nu Cpf' }}</label>
    <input class="form-control" name="nu_cpf" type="text" id="nu_cpf" value="{{ $caousuario->nu_cpf or ''}}" >
    {!! $errors->first('nu_cpf', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_rg') ? 'has-error' : ''}}">
    <label for="nu_rg" class="control-label">{{ 'Nu Rg' }}</label>
    <input class="form-control" name="nu_rg" type="text" id="nu_rg" value="{{ $caousuario->nu_rg or ''}}" >
    {!! $errors->first('nu_rg', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_orgao_emissor') ? 'has-error' : ''}}">
    <label for="no_orgao_emissor" class="control-label">{{ 'No Orgao Emissor' }}</label>
    <input class="form-control" name="no_orgao_emissor" type="text" id="no_orgao_emissor" value="{{ $caousuario->no_orgao_emissor or ''}}" >
    {!! $errors->first('no_orgao_emissor', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('uf_orgao_emissor') ? 'has-error' : ''}}">
    <label for="uf_orgao_emissor" class="control-label">{{ 'Uf Orgao Emissor' }}</label>
    <input class="form-control" name="uf_orgao_emissor" type="text" id="uf_orgao_emissor" value="{{ $caousuario->uf_orgao_emissor or ''}}" >
    {!! $errors->first('uf_orgao_emissor', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_endereco') ? 'has-error' : ''}}">
    <label for="ds_endereco" class="control-label">{{ 'Ds Endereco' }}</label>
    <input class="form-control" name="ds_endereco" type="text" id="ds_endereco" value="{{ $caousuario->ds_endereco or ''}}" >
    {!! $errors->first('ds_endereco', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_email') ? 'has-error' : ''}}">
    <label for="no_email" class="control-label">{{ 'No Email' }}</label>
    <input class="form-control" name="no_email" type="text" id="no_email" value="{{ $caousuario->no_email or ''}}" >
    {!! $errors->first('no_email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_email_pessoal') ? 'has-error' : ''}}">
    <label for="no_email_pessoal" class="control-label">{{ 'No Email Pessoal' }}</label>
    <input class="form-control" name="no_email_pessoal" type="text" id="no_email_pessoal" value="{{ $caousuario->no_email_pessoal or ''}}" >
    {!! $errors->first('no_email_pessoal', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_telefone') ? 'has-error' : ''}}">
    <label for="nu_telefone" class="control-label">{{ 'Nu Telefone' }}</label>
    <input class="form-control" name="nu_telefone" type="text" id="nu_telefone" value="{{ $caousuario->nu_telefone or ''}}" >
    {!! $errors->first('nu_telefone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_alteracao') ? 'has-error' : ''}}">
    <label for="dt_alteracao" class="control-label">{{ 'Dt Alteracao' }}</label>
    <input class="form-control" name="dt_alteracao" type="datetime-local" id="dt_alteracao" value="{{ $caousuario->dt_alteracao or ''}}" >
    {!! $errors->first('dt_alteracao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('url_foto') ? 'has-error' : ''}}">
    <label for="url_foto" class="control-label">{{ 'Url Foto' }}</label>
    <input class="form-control" name="url_foto" type="text" id="url_foto" value="{{ $caousuario->url_foto or ''}}" >
    {!! $errors->first('url_foto', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('instant_messenger') ? 'has-error' : ''}}">
    <label for="instant_messenger" class="control-label">{{ 'Instant Messenger' }}</label>
    <input class="form-control" name="instant_messenger" type="text" id="instant_messenger" value="{{ $caousuario->instant_messenger or ''}}" >
    {!! $errors->first('instant_messenger', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('icq') ? 'has-error' : ''}}">
    <label for="icq" class="control-label">{{ 'Icq' }}</label>
    <input class="form-control" name="icq" type="number" id="icq" value="{{ $caousuario->icq or ''}}" >
    {!! $errors->first('icq', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('msn') ? 'has-error' : ''}}">
    <label for="msn" class="control-label">{{ 'Msn' }}</label>
    <input class="form-control" name="msn" type="text" id="msn" value="{{ $caousuario->msn or ''}}" >
    {!! $errors->first('msn', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('yms') ? 'has-error' : ''}}">
    <label for="yms" class="control-label">{{ 'Yms' }}</label>
    <input class="form-control" name="yms" type="text" id="yms" value="{{ $caousuario->yms or ''}}" >
    {!! $errors->first('yms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_comp_end') ? 'has-error' : ''}}">
    <label for="ds_comp_end" class="control-label">{{ 'Ds Comp End' }}</label>
    <input class="form-control" name="ds_comp_end" type="text" id="ds_comp_end" value="{{ $caousuario->ds_comp_end or ''}}" >
    {!! $errors->first('ds_comp_end', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_bairro') ? 'has-error' : ''}}">
    <label for="ds_bairro" class="control-label">{{ 'Ds Bairro' }}</label>
    <input class="form-control" name="ds_bairro" type="text" id="ds_bairro" value="{{ $caousuario->ds_bairro or ''}}" >
    {!! $errors->first('ds_bairro', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_cep') ? 'has-error' : ''}}">
    <label for="nu_cep" class="control-label">{{ 'Nu Cep' }}</label>
    <input class="form-control" name="nu_cep" type="text" id="nu_cep" value="{{ $caousuario->nu_cep or ''}}" >
    {!! $errors->first('nu_cep', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_cidade') ? 'has-error' : ''}}">
    <label for="no_cidade" class="control-label">{{ 'No Cidade' }}</label>
    <input class="form-control" name="no_cidade" type="text" id="no_cidade" value="{{ $caousuario->no_cidade or ''}}" >
    {!! $errors->first('no_cidade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('uf_cidade') ? 'has-error' : ''}}">
    <label for="uf_cidade" class="control-label">{{ 'Uf Cidade' }}</label>
    <input class="form-control" name="uf_cidade" type="text" id="uf_cidade" value="{{ $caousuario->uf_cidade or ''}}" >
    {!! $errors->first('uf_cidade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_expedicao') ? 'has-error' : ''}}">
    <label for="dt_expedicao" class="control-label">{{ 'Dt Expedicao' }}</label>
    <input class="form-control" name="dt_expedicao" type="date" id="dt_expedicao" value="{{ $caousuario->dt_expedicao or ''}}" >
    {!! $errors->first('dt_expedicao', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

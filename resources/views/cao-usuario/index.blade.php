@extends('layouts.app')
<style media="screen">
.multiselect {
  min-width: 600px;
}
</style>
@section('content')
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-consultor" data-toggle="tab" href="#consultor" role="tab" aria-controls="consultor" aria-selected="true">Consultor</a>
    <a class="nav-item nav-link" id="nav-cliente" data-toggle="tab" href="#cliente" role="tab" aria-controls="cliente" aria-selected="false">Cliente</a>
  </div>
</nav>

<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="consultor" role="tabpanel" aria-labelledby="nav-consultor">
    <div class="row">
      <div class="col-md-8">
        <div class="card-body">
          <div class="mx-1 row">
            <label class="mt-1" for="">Período de </label>
            <select class="ml-1 col-md-2 form-control" name="mes_inicial">
              @include('layouts.selects.mes')
            </select>
            <select class="ml-1 col-md-2 form-control" name="ano_inicial">
              @include('layouts.selects.ano')
            </select>
            <label class="mx-1 mt-1" for="">à</label>
            <select class="ml-1 col-md-2 form-control" name="mes_final">
              @include('layouts.selects.mes')
            </select>
            <select class=" ml-1 col-md-2 form-control" name="ano_final">
              @include('layouts.selects.ano')
            </select>
          </div>
          <br>
          <br>

          <div class="row">
            <select class="col-md-12" id="multiple-consultor" multiple="multiple">
              @for($i = 0; $i < count($dados['consultor']); $i++)
              <option value="{{ $dados['consultor'][$i]['co_usuario'] }}">{{ $dados['consultor'][$i]['no_usuario'] }}</option>
              @endfor
            </select>
          </div>
          <br>
        </div>
        <div id="relatorio-consultor" class="relatorio-consultor"></div>
        <canvas width="400" height="400" id="grafico-consultor" style="max-width: 500px;"></canvas>
      </div>
      <div class="mx-auto col-md-2">
        <button onclick="geraRelatorio('consultor')" class="mx-1 mb-2 mt-5 col-md-10 btn-save btn btn-primary btn-sm">Relatório</button>
        <button onclick="geraGrafico('consultor', 'barra')" class="mx-1 mb-2 col-md-10 btn-save btn btn-primary btn-sm">Gráfico</button>
        <button onclick="geraGrafico('consultor', 'pizza')" class="mx-1 mb-2 col-md-10 btn-save btn btn-primary btn-sm">Gráfico Pizza</button>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="cliente" role="tabpanel" aria-labelledby="nav-cliente">
    <div class="row">
      <div class="col-md-8">
        <div class="card-body">
          <div class="mx-1 row">
            <label class="mt-1" for="">Período de </label>
            <select class="ml-1 col-md-2 form-control" name="mes_inicial_cliente">
              @include('layouts.selects.mes')
            </select>
            <select class="ml-1 col-md-2 form-control" name="ano_inicial_cliente">
              @include('layouts.selects.ano')
            </select>
            <label class="mx-1 mt-1" for="">à</label>
            <select class="ml-1 col-md-2 form-control" name="mes_final_cliente">
              @include('layouts.selects.mes')
            </select>
            <select class=" ml-1 col-md-2 form-control" name="ano_final_cliente">
              @include('layouts.selects.ano')
            </select>
          </div>
          <br>
          <br>
          <div class="row">
            <select class="col-md-12" id="multiple-cliente" multiple="multiple">
              @for($i = 0; $i < count($dados['cliente']); $i++)
              <option value="{{ $dados['cliente'][$i]['co_cliente'] }}">{{ $dados['cliente'][$i]['no_fantasia'] }}</option>
              @endfor
            </select>
          </div>
          <br>
        </div>
        <div id="relatorio-cliente" class="relatorio-cliente"></div>
        <canvas width="400" height="400" id="grafico-cliente" style="max-width: 500px;"></canvas>
      </div>
      <div class="mx-auto col-md-2">
        <button onclick="geraRelatorio('cliente')" class="mx-1 mb-2 mt-5 col-md-10 btn-save btn btn-primary btn-sm">Relatório</button>
        <button onclick="geraGrafico('cliente', 'barra')" class="mx-1 mb-2 col-md-10 btn-save btn btn-primary btn-sm">Gráfico</button>
        <button onclick="geraGrafico('cliente', 'pizza')" class="mx-1 mb-2 col-md-10 btn-save btn btn-primary btn-sm">Gráfico Pizza</button>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

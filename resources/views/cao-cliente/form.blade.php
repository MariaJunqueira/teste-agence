<div class="form-group {{ $errors->has('co_cliente') ? 'has-error' : ''}}">
    <label for="co_cliente" class="control-label">{{ 'Co Cliente' }}</label>
    <input class="form-control" name="co_cliente" type="number" id="co_cliente" value="{{ $caocliente->co_cliente or ''}}" >
    {!! $errors->first('co_cliente', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_razao') ? 'has-error' : ''}}">
    <label for="no_razao" class="control-label">{{ 'No Razao' }}</label>
    <input class="form-control" name="no_razao" type="text" id="no_razao" value="{{ $caocliente->no_razao or ''}}" >
    {!! $errors->first('no_razao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_fantasia') ? 'has-error' : ''}}">
    <label for="no_fantasia" class="control-label">{{ 'No Fantasia' }}</label>
    <input class="form-control" name="no_fantasia" type="text" id="no_fantasia" value="{{ $caocliente->no_fantasia or ''}}" >
    {!! $errors->first('no_fantasia', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_contato') ? 'has-error' : ''}}">
    <label for="no_contato" class="control-label">{{ 'No Contato' }}</label>
    <input class="form-control" name="no_contato" type="text" id="no_contato" value="{{ $caocliente->no_contato or ''}}" >
    {!! $errors->first('no_contato', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_telefone') ? 'has-error' : ''}}">
    <label for="nu_telefone" class="control-label">{{ 'Nu Telefone' }}</label>
    <input class="form-control" name="nu_telefone" type="text" id="nu_telefone" value="{{ $caocliente->nu_telefone or ''}}" >
    {!! $errors->first('nu_telefone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_ramal') ? 'has-error' : ''}}">
    <label for="nu_ramal" class="control-label">{{ 'Nu Ramal' }}</label>
    <input class="form-control" name="nu_ramal" type="number" id="nu_ramal" value="{{ $caocliente->nu_ramal or ''}}" >
    {!! $errors->first('nu_ramal', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_cnpj') ? 'has-error' : ''}}">
    <label for="nu_cnpj" class="control-label">{{ 'Nu Cnpj' }}</label>
    <input class="form-control" name="nu_cnpj" type="text" id="nu_cnpj" value="{{ $caocliente->nu_cnpj or ''}}" >
    {!! $errors->first('nu_cnpj', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_endereco') ? 'has-error' : ''}}">
    <label for="ds_endereco" class="control-label">{{ 'Ds Endereco' }}</label>
    <input class="form-control" name="ds_endereco" type="text" id="ds_endereco" value="{{ $caocliente->ds_endereco or ''}}" >
    {!! $errors->first('ds_endereco', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_numero') ? 'has-error' : ''}}">
    <label for="nu_numero" class="control-label">{{ 'Nu Numero' }}</label>
    <input class="form-control" name="nu_numero" type="number" id="nu_numero" value="{{ $caocliente->nu_numero or ''}}" >
    {!! $errors->first('nu_numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_complemento') ? 'has-error' : ''}}">
    <label for="ds_complemento" class="control-label">{{ 'Ds Complemento' }}</label>
    <input class="form-control" name="ds_complemento" type="text" id="ds_complemento" value="{{ $caocliente->ds_complemento or ''}}" >
    {!! $errors->first('ds_complemento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_bairro') ? 'has-error' : ''}}">
    <label for="no_bairro" class="control-label">{{ 'No Bairro' }}</label>
    <input class="form-control" name="no_bairro" type="text" id="no_bairro" value="{{ $caocliente->no_bairro or ''}}" >
    {!! $errors->first('no_bairro', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_cep') ? 'has-error' : ''}}">
    <label for="nu_cep" class="control-label">{{ 'Nu Cep' }}</label>
    <input class="form-control" name="nu_cep" type="text" id="nu_cep" value="{{ $caocliente->nu_cep or ''}}" >
    {!! $errors->first('nu_cep', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_pais') ? 'has-error' : ''}}">
    <label for="no_pais" class="control-label">{{ 'No Pais' }}</label>
    <input class="form-control" name="no_pais" type="text" id="no_pais" value="{{ $caocliente->no_pais or ''}}" >
    {!! $errors->first('no_pais', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_ramo') ? 'has-error' : ''}}">
    <label for="co_ramo" class="control-label">{{ 'Co Ramo' }}</label>
    <input class="form-control" name="co_ramo" type="number" id="co_ramo" value="{{ $caocliente->co_ramo or ''}}" >
    {!! $errors->first('co_ramo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_cidade') ? 'has-error' : ''}}">
    <label for="co_cidade" class="control-label">{{ 'Co Cidade' }}</label>
    <input class="form-control" name="co_cidade" type="number" id="co_cidade" value="{{ $caocliente->co_cidade or ''}}" >
    {!! $errors->first('co_cidade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_status') ? 'has-error' : ''}}">
    <label for="co_status" class="control-label">{{ 'Co Status' }}</label>
    <input class="form-control" name="co_status" type="text" id="co_status" value="{{ $caocliente->co_status or ''}}" >
    {!! $errors->first('co_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_site') ? 'has-error' : ''}}">
    <label for="ds_site" class="control-label">{{ 'Ds Site' }}</label>
    <input class="form-control" name="ds_site" type="text" id="ds_site" value="{{ $caocliente->ds_site or ''}}" >
    {!! $errors->first('ds_site', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_email') ? 'has-error' : ''}}">
    <label for="ds_email" class="control-label">{{ 'Ds Email' }}</label>
    <input class="form-control" name="ds_email" type="text" id="ds_email" value="{{ $caocliente->ds_email or ''}}" >
    {!! $errors->first('ds_email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_cargo_contato') ? 'has-error' : ''}}">
    <label for="ds_cargo_contato" class="control-label">{{ 'Ds Cargo Contato' }}</label>
    <input class="form-control" name="ds_cargo_contato" type="text" id="ds_cargo_contato" value="{{ $caocliente->ds_cargo_contato or ''}}" >
    {!! $errors->first('ds_cargo_contato', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tp_cliente') ? 'has-error' : ''}}">
    <label for="tp_cliente" class="control-label">{{ 'Tp Cliente' }}</label>
    <input class="form-control" name="tp_cliente" type="text" id="tp_cliente" value="{{ $caocliente->tp_cliente or ''}}" >
    {!! $errors->first('tp_cliente', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_referencia') ? 'has-error' : ''}}">
    <label for="ds_referencia" class="control-label">{{ 'Ds Referencia' }}</label>
    <input class="form-control" name="ds_referencia" type="text" id="ds_referencia" value="{{ $caocliente->ds_referencia or ''}}" >
    {!! $errors->first('ds_referencia', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_complemento_status') ? 'has-error' : ''}}">
    <label for="co_complemento_status" class="control-label">{{ 'Co Complemento Status' }}</label>
    <input class="form-control" name="co_complemento_status" type="number" id="co_complemento_status" value="{{ $caocliente->co_complemento_status or ''}}" >
    {!! $errors->first('co_complemento_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_fax') ? 'has-error' : ''}}">
    <label for="nu_fax" class="control-label">{{ 'Nu Fax' }}</label>
    <input class="form-control" name="nu_fax" type="text" id="nu_fax" value="{{ $caocliente->nu_fax or ''}}" >
    {!! $errors->first('nu_fax', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ddd2') ? 'has-error' : ''}}">
    <label for="ddd2" class="control-label">{{ 'Ddd2' }}</label>
    <input class="form-control" name="ddd2" type="number" id="ddd2" value="{{ $caocliente->ddd2 or ''}}" >
    {!! $errors->first('ddd2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telefone2') ? 'has-error' : ''}}">
    <label for="telefone2" class="control-label">{{ 'Telefone2' }}</label>
    <input class="form-control" name="telefone2" type="text" id="telefone2" value="{{ $caocliente->telefone2 or ''}}" >
    {!! $errors->first('telefone2', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

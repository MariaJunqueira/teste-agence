@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">CaoCliente {{ $caocliente->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/cao-cliente') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/cao-cliente/' . $caocliente->id . '/edit') }}" title="Edit CaoCliente"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('caocliente' . '/' . $caocliente->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete CaoCliente" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $caocliente->id }}</td>
                                    </tr>
                                    <tr><th> Co Cliente </th><td> {{ $caocliente->co_cliente }} </td></tr><tr><th> No Razao </th><td> {{ $caocliente->no_razao }} </td></tr><tr><th> No Fantasia </th><td> {{ $caocliente->no_fantasia }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<?php

function ConvertDate($date)
{
  if($date != "") return date("d/m/Y" , strtotime($date));
}

function ConvertTime($time)
{
  if($time != "") return date("H:i" , strtotime($time));
}

function ConvertDateTime($dateTime)
{
  if($dateTime != "") return date("d/m/Y H:i" , strtotime($dateTime));
}

function ConvertMoney($money)
{
  return "R$ " . number_format($money, 2,",",".");
}


?>

@include('globalfunctions')
<style media="screen">
table, th, th, tr, td {
  border: 1px solid black;
}
</style>

<table class="mx-1 mt-2 col-md-11 table table-striped">
  <thead>
  </thead>
  <tbody>
    @if(isset($relatorio['fatura'][0]) && count($relatorio['fatura']) > 0)
    <tr>
      <td> # </td>
      @foreach ($meses as $key => $value)
      @for($k = 0; $k < count($value); $k++)
      <td> {{ sprintf("%02d", $value[$k]) }}/{{ $key }} </td>
      @endfor
      @endforeach
    </tr>

    @for($i = 0; $i < count($relatorio['fatura']); $i++)
    @if(isset($relatorio['fatura'][$i]) && count($relatorio['fatura'][$i]['receita_liquida']) > 0)
    @php
    $array = $relatorio['fatura'][$i]['receita_liquida'];
    $array_key = array_keys($array);

    $total_receita = 0;
    @endphp
    @if( isset($relatorio['fatura'][$i][0]['no_fantasia']) > 0)
    <tr>
      <td>{{ $relatorio['fatura'][$i][0]['no_fantasia'] }}</td>
      @foreach ($array as $key => $value)
      <td> {{ ConvertMoney($value) }}</td>
      @endforeach
    </tr>
    @endif
    @endif
    @endfor
    @else
    <tr>
      <td colspan="">Não há valores para este período</td>
    </tr>
    @endif
  </tbody>
</table>

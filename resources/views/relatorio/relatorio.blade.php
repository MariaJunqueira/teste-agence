@include('globalfunctions')

@for($i = 0; $i < count($relatorio['fatura']); $i++)
@if( count($relatorio['fatura'][$i]['receita_liquida']) > 0)
<table class="mx-1 mt-2 col-md-11 table table-striped">
  <thead>
    <th colspan="5"><b>{{ $relatorio['fatura'][$i][0]['no_usuario'] }}</b></th>
  </thead>
  <tbody>
    <tr>
      <td><b>Período</b></td>
      <td><b>Receita Líquida</b></td>
      <td><b>Custo Fixo</b></td>
      <td><b>Comissão</b></td>
      <td><b>Lucro</b></td>
    </tr>
    @php
    $array = $relatorio['fatura'][$i]['receita_liquida'];
    $salario = $relatorio['fatura'][$i]['custo_fixo'];
    $total_receita = 0;
    $total_salario = 0;
    $total_comissao = 0;
    $total_lucro = 0;
    @endphp
    @foreach ($array as $key => $value)
    @php
    $comissao = $relatorio['fatura'][$i]['comissao_total'][$key];
    $lucro = $value - ( $salario + $comissao );
    $total_receita += $value;
    $total_salario += $salario;
    $total_comissao += $comissao;
    $total_lucro += $value;
    @endphp
    <tr>
      <td> {{ $key}} </td>
      <td> {{ ConvertMoney($value)}} </td>
      <td> {{ ConvertMoney($salario)}} </td>
      <td> {{ ConvertMoney($comissao)}} </td>
      <td> {{ ConvertMoney($lucro)}} </td>
    </tr>
    @endforeach
    <tr>
      <td> <b>Saldo</b> </td>
      <td> {{ ConvertMoney($total_receita)}} </td>
      <td> {{ ConvertMoney($total_salario)}} </td>
      <td> {{ ConvertMoney($total_comissao)}} </td>
      <td> {{ ConvertMoney($total_lucro)}} </td>
    </tr>
  </tbody>
</table>
@endif
@endfor

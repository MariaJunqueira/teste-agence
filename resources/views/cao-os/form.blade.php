<div class="form-group {{ $errors->has('co_os') ? 'has-error' : ''}}">
    <label for="co_os" class="control-label">{{ 'Co Os' }}</label>
    <input class="form-control" name="co_os" type="number" id="co_os" value="{{ $caoo->co_os or ''}}" >
    {!! $errors->first('co_os', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_os') ? 'has-error' : ''}}">
    <label for="nu_os" class="control-label">{{ 'Nu Os' }}</label>
    <input class="form-control" name="nu_os" type="number" id="nu_os" value="{{ $caoo->nu_os or ''}}" >
    {!! $errors->first('nu_os', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_sistema') ? 'has-error' : ''}}">
    <label for="co_sistema" class="control-label">{{ 'Co Sistema' }}</label>
    <input class="form-control" name="co_sistema" type="number" id="co_sistema" value="{{ $caoo->co_sistema or ''}}" >
    {!! $errors->first('co_sistema', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_usuario') ? 'has-error' : ''}}">
    <label for="co_usuario" class="control-label">{{ 'Co Usuario' }}</label>
    <input class="form-control" name="co_usuario" type="text" id="co_usuario" value="{{ $caoo->co_usuario or ''}}" >
    {!! $errors->first('co_usuario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_arquitetura') ? 'has-error' : ''}}">
    <label for="co_arquitetura" class="control-label">{{ 'Co Arquitetura' }}</label>
    <input class="form-control" name="co_arquitetura" type="number" id="co_arquitetura" value="{{ $caoo->co_arquitetura or ''}}" >
    {!! $errors->first('co_arquitetura', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_os') ? 'has-error' : ''}}">
    <label for="ds_os" class="control-label">{{ 'Ds Os' }}</label>
    <input class="form-control" name="ds_os" type="text" id="ds_os" value="{{ $caoo->ds_os or ''}}" >
    {!! $errors->first('ds_os', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_caracteristica') ? 'has-error' : ''}}">
    <label for="ds_caracteristica" class="control-label">{{ 'Ds Caracteristica' }}</label>
    <input class="form-control" name="ds_caracteristica" type="text" id="ds_caracteristica" value="{{ $caoo->ds_caracteristica or ''}}" >
    {!! $errors->first('ds_caracteristica', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ds_requisito') ? 'has-error' : ''}}">
    <label for="ds_requisito" class="control-label">{{ 'Ds Requisito' }}</label>
    <input class="form-control" name="ds_requisito" type="text" id="ds_requisito" value="{{ $caoo->ds_requisito or ''}}" >
    {!! $errors->first('ds_requisito', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_inicio') ? 'has-error' : ''}}">
    <label for="dt_inicio" class="control-label">{{ 'Dt Inicio' }}</label>
    <input class="form-control" name="dt_inicio" type="date" id="dt_inicio" value="{{ $caoo->dt_inicio or ''}}" >
    {!! $errors->first('dt_inicio', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_fim') ? 'has-error' : ''}}">
    <label for="dt_fim" class="control-label">{{ 'Dt Fim' }}</label>
    <input class="form-control" name="dt_fim" type="date" id="dt_fim" value="{{ $caoo->dt_fim or ''}}" >
    {!! $errors->first('dt_fim', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_status') ? 'has-error' : ''}}">
    <label for="co_status" class="control-label">{{ 'Co Status' }}</label>
    <input class="form-control" name="co_status" type="number" id="co_status" value="{{ $caoo->co_status or ''}}" >
    {!! $errors->first('co_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('diretoria_sol') ? 'has-error' : ''}}">
    <label for="diretoria_sol" class="control-label">{{ 'Diretoria Sol' }}</label>
    <input class="form-control" name="diretoria_sol" type="text" id="diretoria_sol" value="{{ $caoo->diretoria_sol or ''}}" >
    {!! $errors->first('diretoria_sol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_sol') ? 'has-error' : ''}}">
    <label for="dt_sol" class="control-label">{{ 'Dt Sol' }}</label>
    <input class="form-control" name="dt_sol" type="date" id="dt_sol" value="{{ $caoo->dt_sol or ''}}" >
    {!! $errors->first('dt_sol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nu_tel_sol') ? 'has-error' : ''}}">
    <label for="nu_tel_sol" class="control-label">{{ 'Nu Tel Sol' }}</label>
    <input class="form-control" name="nu_tel_sol" type="text" id="nu_tel_sol" value="{{ $caoo->nu_tel_sol or ''}}" >
    {!! $errors->first('nu_tel_sol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ddd_tel_sol') ? 'has-error' : ''}}">
    <label for="ddd_tel_sol" class="control-label">{{ 'Ddd Tel Sol' }}</label>
    <input class="form-control" name="ddd_tel_sol" type="text" id="ddd_tel_sol" value="{{ $caoo->ddd_tel_sol or ''}}" >
    {!! $errors->first('ddd_tel_sol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('usuario_sol') ? 'has-error' : ''}}">
    <label for="usuario_sol" class="control-label">{{ 'Usuario Sol' }}</label>
    <input class="form-control" name="usuario_sol" type="text" id="usuario_sol" value="{{ $caoo->usuario_sol or ''}}" >
    {!! $errors->first('usuario_sol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_imp') ? 'has-error' : ''}}">
    <label for="dt_imp" class="control-label">{{ 'Dt Imp' }}</label>
    <input class="form-control" name="dt_imp" type="date" id="dt_imp" value="{{ $caoo->dt_imp or ''}}" >
    {!! $errors->first('dt_imp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_garantia') ? 'has-error' : ''}}">
    <label for="dt_garantia" class="control-label">{{ 'Dt Garantia' }}</label>
    <input class="form-control" name="dt_garantia" type="date" id="dt_garantia" value="{{ $caoo->dt_garantia or ''}}" >
    {!! $errors->first('dt_garantia', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_email') ? 'has-error' : ''}}">
    <label for="co_email" class="control-label">{{ 'Co Email' }}</label>
    <input class="form-control" name="co_email" type="text" id="co_email" value="{{ $caoo->co_email or ''}}" >
    {!! $errors->first('co_email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_os_prospect_rel') ? 'has-error' : ''}}">
    <label for="co_os_prospect_rel" class="control-label">{{ 'Co Os Prospect Rel' }}</label>
    <input class="form-control" name="co_os_prospect_rel" type="number" id="co_os_prospect_rel" value="{{ $caoo->co_os_prospect_rel or ''}}" >
    {!! $errors->first('co_os_prospect_rel', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

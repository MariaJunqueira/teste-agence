<div class="form-group {{ $errors->has('co_usuario') ? 'has-error' : ''}}">
    <label for="co_usuario" class="control-label">{{ 'Co Usuario' }}</label>
    <input class="form-control" name="co_usuario" type="text" id="co_usuario" value="{{ $caosalario->co_usuario or ''}}" >
    {!! $errors->first('co_usuario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_alteracao_cn') ? 'has-error' : ''}}">
    <label for="dt_alteracao_cn" class="control-label">{{ 'Dt Alteracao Cn' }}</label>
    <input class="form-control" name="dt_alteracao_cn" type="date" id="dt_alteracao_cn" value="{{ $caosalario->dt_alteracao_cn or ''}}" >
    {!! $errors->first('dt_alteracao_cn', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('brut_salario') ? 'has-error' : ''}}">
    <label for="brut_salario" class="control-label">{{ 'Brut Salario' }}</label>
    <input class="form-control" name="brut_salario" type="number" id="brut_salario" value="{{ $caosalario->brut_salario or ''}}" >
    {!! $errors->first('brut_salario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('liq_salario') ? 'has-error' : ''}}">
    <label for="liq_salario" class="control-label">{{ 'Liq Salario' }}</label>
    <input class="form-control" name="liq_salario" type="number" id="liq_salario" value="{{ $caosalario->liq_salario or ''}}" >
    {!! $errors->first('liq_salario', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

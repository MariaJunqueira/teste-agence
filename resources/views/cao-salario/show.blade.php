@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">CaoSalario {{ $caosalario->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/cao-salario') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/cao-salario/' . $caosalario->id . '/edit') }}" title="Edit CaoSalario"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('caosalario' . '/' . $caosalario->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete CaoSalario" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $caosalario->id }}</td>
                                    </tr>
                                    <tr><th> Co Usuario </th><td> {{ $caosalario->co_usuario }} </td></tr><tr><th> Dt Alteracao Cn </th><td> {{ $caosalario->dt_alteracao_cn }} </td></tr><tr><th> Brut Salario </th><td> {{ $caosalario->brut_salario }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

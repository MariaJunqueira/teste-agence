@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">PermissaoSistema {{ $permissaosistema->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/permissao-sistema') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/permissao-sistema/' . $permissaosistema->id . '/edit') }}" title="Edit PermissaoSistema"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('permissaosistema' . '/' . $permissaosistema->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete PermissaoSistema" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $permissaosistema->id }}</td>
                                    </tr>
                                    <tr><th> Co Usuario </th><td> {{ $permissaosistema->co_usuario }} </td></tr><tr><th> Co Tipo Usuario </th><td> {{ $permissaosistema->co_tipo_usuario }} </td></tr><tr><th> Co Sistema </th><td> {{ $permissaosistema->co_sistema }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="form-group {{ $errors->has('co_usuario') ? 'has-error' : ''}}">
    <label for="co_usuario" class="control-label">{{ 'Co Usuario' }}</label>
    <input class="form-control" name="co_usuario" type="text" id="co_usuario" value="{{ $permissaosistema->co_usuario or ''}}" >
    {!! $errors->first('co_usuario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_tipo_usuario') ? 'has-error' : ''}}">
    <label for="co_tipo_usuario" class="control-label">{{ 'Co Tipo Usuario' }}</label>
    <input class="form-control" name="co_tipo_usuario" type="number" id="co_tipo_usuario" value="{{ $permissaosistema->co_tipo_usuario or ''}}" >
    {!! $errors->first('co_tipo_usuario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_sistema') ? 'has-error' : ''}}">
    <label for="co_sistema" class="control-label">{{ 'Co Sistema' }}</label>
    <input class="form-control" name="co_sistema" type="number" id="co_sistema" value="{{ $permissaosistema->co_sistema or ''}}" >
    {!! $errors->first('co_sistema', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('in_ativo') ? 'has-error' : ''}}">
    <label for="in_ativo" class="control-label">{{ 'In Ativo' }}</label>
    <input class="form-control" name="in_ativo" type="text" id="in_ativo" value="{{ $permissaosistema->in_ativo or ''}}" >
    {!! $errors->first('in_ativo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_usuario_atualizacao') ? 'has-error' : ''}}">
    <label for="co_usuario_atualizacao" class="control-label">{{ 'Co Usuario Atualizacao' }}</label>
    <input class="form-control" name="co_usuario_atualizacao" type="text" id="co_usuario_atualizacao" value="{{ $permissaosistema->co_usuario_atualizacao or ''}}" >
    {!! $errors->first('co_usuario_atualizacao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_atualizacao') ? 'has-error' : ''}}">
    <label for="dt_atualizacao" class="control-label">{{ 'Dt Atualizacao' }}</label>
    <input class="form-control" name="dt_atualizacao" type="datetime-local" id="dt_atualizacao" value="{{ $permissaosistema->dt_atualizacao or ''}}" >
    {!! $errors->first('dt_atualizacao', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

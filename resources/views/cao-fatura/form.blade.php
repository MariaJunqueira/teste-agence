<div class="form-group {{ $errors->has('co_fatura') ? 'has-error' : ''}}">
    <label for="co_fatura" class="control-label">{{ 'Co Fatura' }}</label>
    <input class="form-control" name="co_fatura" type="number" id="co_fatura" value="{{ $caofatura->co_fatura or ''}}" >
    {!! $errors->first('co_fatura', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_cliente') ? 'has-error' : ''}}">
    <label for="co_cliente" class="control-label">{{ 'Co Cliente' }}</label>
    <input class="form-control" name="co_cliente" type="number" id="co_cliente" value="{{ $caofatura->co_cliente or ''}}" >
    {!! $errors->first('co_cliente', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_sistema') ? 'has-error' : ''}}">
    <label for="co_sistema" class="control-label">{{ 'Co Sistema' }}</label>
    <input class="form-control" name="co_sistema" type="number" id="co_sistema" value="{{ $caofatura->co_sistema or ''}}" >
    {!! $errors->first('co_sistema', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('co_os') ? 'has-error' : ''}}">
    <label for="co_os" class="control-label">{{ 'Co Os' }}</label>
    <input class="form-control" name="co_os" type="number" id="co_os" value="{{ $caofatura->co_os or ''}}" >
    {!! $errors->first('co_os', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('num_nf') ? 'has-error' : ''}}">
    <label for="num_nf" class="control-label">{{ 'Num Nf' }}</label>
    <input class="form-control" name="num_nf" type="number" id="num_nf" value="{{ $caofatura->num_nf or ''}}" >
    {!! $errors->first('num_nf', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('total') ? 'has-error' : ''}}">
    <label for="total" class="control-label">{{ 'Total' }}</label>
    <input class="form-control" name="total" type="number" id="total" value="{{ $caofatura->total or ''}}" >
    {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('valor') ? 'has-error' : ''}}">
    <label for="valor" class="control-label">{{ 'Valor' }}</label>
    <input class="form-control" name="valor" type="number" id="valor" value="{{ $caofatura->valor or ''}}" >
    {!! $errors->first('valor', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dt_emissao') ? 'has-error' : ''}}">
    <label for="dt_emissao" class="control-label">{{ 'Dt Emissao' }}</label>
    <input class="form-control" name="dt_emissao" type="date" id="dt_emissao" value="{{ $caofatura->dt_emissao or ''}}" >
    {!! $errors->first('dt_emissao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('corpo_nf') ? 'has-error' : ''}}">
    <label for="corpo_nf" class="control-label">{{ 'Corpo Nf' }}</label>
    <input class="form-control" name="corpo_nf" type="text" id="corpo_nf" value="{{ $caofatura->corpo_nf or ''}}" >
    {!! $errors->first('corpo_nf', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('comissao_cn') ? 'has-error' : ''}}">
    <label for="comissao_cn" class="control-label">{{ 'Comissao Cn' }}</label>
    <input class="form-control" name="comissao_cn" type="number" id="comissao_cn" value="{{ $caofatura->comissao_cn or ''}}" >
    {!! $errors->first('comissao_cn', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('total_imp_inc') ? 'has-error' : ''}}">
    <label for="total_imp_inc" class="control-label">{{ 'Total Imp Inc' }}</label>
    <input class="form-control" name="total_imp_inc" type="number" id="total_imp_inc" value="{{ $caofatura->total_imp_inc or ''}}" >
    {!! $errors->first('total_imp_inc', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

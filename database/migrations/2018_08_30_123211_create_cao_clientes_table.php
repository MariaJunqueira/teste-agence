<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaoClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cao_clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('co_cliente')->nullable();
            $table->string('no_razao')->nullable();
            $table->string('no_fantasia')->nullable();
            $table->string('no_contato')->nullable();
            $table->string('nu_telefone')->nullable();
            $table->integer('nu_ramal')->nullable();
            $table->string('nu_cnpj')->nullable();
            $table->string('ds_endereco')->nullable();
            $table->integer('nu_numero')->nullable();
            $table->string('ds_complemento')->nullable();
            $table->string('no_bairro')->nullable();
            $table->string('nu_cep')->nullable();
            $table->string('no_pais')->nullable();
            $table->integer('co_ramo')->nullable();
            $table->integer('co_cidade')->nullable();
            $table->string('co_status')->nullable();
            $table->string('ds_site')->nullable();
            $table->string('ds_email')->nullable();
            $table->string('ds_cargo_contato')->nullable();
            $table->string('tp_cliente')->nullable();
            $table->string('ds_referencia')->nullable();
            $table->integer('co_complemento_status')->nullable();
            $table->string('nu_fax')->nullable();
            $table->integer('ddd2')->nullable();
            $table->string('telefone2')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cao_clientes');
    }
}

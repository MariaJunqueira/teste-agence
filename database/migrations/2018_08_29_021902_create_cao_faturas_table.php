<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaoFaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cao_faturas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('co_fatura')->nullable();
            $table->integer('co_cliente')->nullable();
            $table->integer('co_sistema')->nullable();
            $table->integer('co_os')->nullable();
            $table->integer('num_nf')->nullable();
            $table->float('total')->nullable();
            $table->float('valor')->nullable();
            $table->date('dt_emissao')->nullable();
            $table->string('corpo_nf')->nullable();
            $table->float('comissao_cn')->nullable();
            $table->float('total_imp_inc')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cao_faturas');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaoSalariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cao_salarios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('co_usuario')->nullable();
            $table->date('dt_alteracao_cn')->nullable();
            $table->float('brut_salario')->nullable();
            $table->float('liq_salario')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cao_salarios');
    }
}

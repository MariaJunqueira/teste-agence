<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaoOsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cao_os', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('co_os')->nullable();
            $table->integer('nu_os')->nullable();
            $table->integer('co_sistema')->nullable();
            $table->string('co_usuario')->nullable();
            $table->integer('co_arquitetura')->nullable();
            $table->string('ds_os')->nullable();
            $table->string('ds_caracteristica')->nullable();
            $table->string('ds_requisito')->nullable();
            $table->date('dt_inicio')->nullable();
            $table->date('dt_fim')->nullable();
            $table->integer('co_status')->nullable();
            $table->string('diretoria_sol')->nullable();
            $table->date('dt_sol')->nullable();
            $table->string('nu_tel_sol')->nullable();
            $table->string('ddd_tel_sol')->nullable();
            $table->string('usuario_sol')->nullable();
            $table->date('dt_imp')->nullable();
            $table->date('dt_garantia')->nullable();
            $table->string('co_email')->nullable();
            $table->integer('co_os_prospect_rel')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cao_os');
    }
}

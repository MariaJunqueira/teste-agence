<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaoUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cao_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('co_usuario')->nullable();
            $table->string('ds_senha')->nullable();
            $table->string('co_usuario_autorizacao')->nullable();
            $table->integer('nu_matricula')->nullable();
            $table->date('dt_nascimento')->nullable();
            $table->date('dt_admissao_empresa')->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->date('dt_expiracao')->nullable();
            $table->string('nu_cpf')->nullable();
            $table->string('nu_rg')->nullable();
            $table->string('no_orgao_emissor')->nullable();
            $table->string('uf_orgao_emissor')->nullable();
            $table->string('ds_endereco')->nullable();
            $table->string('no_email')->nullable();
            $table->string('no_email_pessoal')->nullable();
            $table->string('nu_telefone')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('url_foto')->nullable();
            $table->string('instant_messenger')->nullable();
            $table->integer('icq')->nullable();
            $table->string('msn')->nullable();
            $table->string('yms')->nullable();
            $table->string('ds_comp_end')->nullable();
            $table->string('ds_bairro')->nullable();
            $table->string('nu_cep')->nullable();
            $table->string('no_cidade')->nullable();
            $table->string('uf_cidade')->nullable();
            $table->date('dt_expedicao')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cao_usuarios');
    }
}

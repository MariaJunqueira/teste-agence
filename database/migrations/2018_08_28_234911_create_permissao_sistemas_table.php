<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePermissaoSistemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissao_sistemas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('co_usuario')->nullable();
            $table->integer('co_tipo_usuario')->nullable();
            $table->integer('co_sistema')->nullable();
            $table->string('in_ativo')->nullable();
            $table->string('co_usuario_atualizacao')->nullable();
            $table->dateTime('dt_atualizacao')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permissao_sistemas');
    }
}

<?php

Route::get('/', function () {
    return view('welcome');
});

Route::resource('cao-usuario', 'CaoUsuarioController');
Route::resource('cao-os', 'CaoOsController');
Route::resource('cao-fatura', 'CaoFaturaController');
// Route::resource('permissao-sistema', 'PermissaoSistemaController');

Route::get('gera-relatorio/{usuarios}/{datas}/{pessoa}/{grafico}', 'RelatorioController@getRelatorio');
Route::get('gera-grafico/{usuarios}/{datas}/{pessoa}', 'RelatorioController@getGrafico');

Route::resource('cao-salario', 'CaoSalarioController');
Route::resource('cao-cliente', 'CaoClienteController');
